import Link from 'next/link'
import Head from '../components/head'
import Nav from '../components/nav'
import axios from 'axios'

const Index = ({ people }) => (
  <div>
    <Head title="Home" />
    <Nav />

    <div className="hero">
      <h1 className="title">Welcome to Next!</h1>
      <p className="description">To get started, edit <code>pages/index.js</code> and save to reload.</p>

      <div className="row">
        {
          people.map(person => (
            <Link href={ `/person?id=${person.id}` }>
              <a className="card">
                <h3>{ person.name }</h3>
              </a>
            </Link>
          ))
        }
      </div>
    </div>

    <style jsx>{`
      .hero {
        width: 100%;
        color: #333;
      }
      .title {
        margin: 0;
        width: 100%;
        padding-top: 80px;
        line-height: 1.15;
        font-size: 48px;
      }
      .title, .description {
        text-align: center;
      }
      .row {
        max-width: 880px;
        margin: 80px auto 40px;
        display: flex;
        flex-direction: row;
        justify-content: space-around;
        flex-wrap: wrap;
      }
      .card {
        padding: 18px 18px 24px;
        margin-top: 20px;
        width: 220px;
        text-align: left;
        text-decoration: none;
        color: #434343;
        border: 1px solid #9B9B9B;
      }
      .card:hover {
        border-color: #067df7;
      }
      .card h3 {
        margin: 0;
        color: #067df7;
        font-size: 18px;
      }
      .card p {
        margin: 0;
        padding: 12px 0 0;
        font-size: 13px;
        color: #333;
      }
    `}</style>
  </div>
)

Index.getInitialProps = async () => {
  const response = await axios.get('https://swapi.co/api/people')
  const people = response.data.results.map((p, idx) => ({ ...p, id: idx+1}))
  return ({
    people
  })
}

export default Index