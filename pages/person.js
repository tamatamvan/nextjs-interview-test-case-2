import axios from 'axios'

const Person = ({ person }) => (
  <div id="person-detail">
    <div className="person-data">
      <h1>{ person.name}</h1>
      <p>
        Height: { person.height }
      </p>
      <p>
        Mass: { person.mass }
      </p>
      <p>
        Gender: { person.gender }
      </p>
    </div>

    <style jsx>{`
      #person-detail {
        height: 100vh;
        width: 100%;
        display: flex;
        align-items: center;
        justify-content: center;
      }
      .person-data {
        width: 70%;
      }
    `}</style>
  </div>
)

Person.getInitialProps = async ({ query: { id } }) => {
  const response = await axios.get('https://swapi.co/api/people/' + id +'/')
  const person = response.data

  return ({
    person
  })
}

export default Person